# MUSICNEWSAPP

Guilhem Fouassier, Théo Mackowiak, Manida Vilay

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Start the json server to access the API
```
yarn server
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
