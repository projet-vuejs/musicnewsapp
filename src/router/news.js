import Index from '@/views/news/Index.vue'
import Show from '@/views/news/Show.vue'
import Create from '@/views/news/Create.vue'
import Edit from '@/views/news/Edit.vue'
import IndexFront from '@/views/news/IndexFront.vue'
import ShowFront from '@/views/news/ShowFront.vue'

const routes = [
  {
    path: 'admin/news',
    name: 'admin.news.index',
    component: Index,
  },
  {
    path: 'admin/news/create',
    name: 'admin.news.create',
    component: Create,
  },
  {
    path: 'admin/news/:id/edit',
    name: 'admin.news.edit',
    component: Edit,
  },
  {
    path: 'admin/news/:id',
    name: 'admin.news.show',
    component: Show,
  },
  {
    path: '/news',
    name: 'news.index',
    component: IndexFront,
  },
  {
    path: '/news/:id',
    name: 'news.show',
    component: ShowFront,
  },
]

export default routes
