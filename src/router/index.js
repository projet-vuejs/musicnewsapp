/* ------------------------------ DEPENDENCIES ------------------------------ */

import Router from 'vue-router'
import Vue from 'vue'

/* ----------------------------------- API ---------------------------------- */

import userApi from '@/api/users'

/* --------------------------------- ROUTERS -------------------------------- */

import newsRoutes from '@/router/news'
import artistsRoutes from '@/router/artists'
import concertsRoutes from '@/router/concerts'
import albumsRoutes from '@/router/albums'

/* --------------------------------- LAYOUT --------------------------------- */

import Main from '@/layouts/Main'

/* ------------------------------- COMPONENTS ------------------------------- */

import Login from '@/components/Login'
import Register from '@/components/Register'
import Homepage from '@/views/Homepage'
import Search from '@/components/Search'

/* ---------------------------------- VIEWS --------------------------------- */

import IndexAdmin from '@/views/IndexAdmin'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      component: Main,
      path: '',
      children: [
        {
          component: Homepage,
          name: 'Homepage',
          path: ''
        },
        {
          component: IndexAdmin,
          name: 'indexAdmin',
          path: 'admin',
          meta: {
            auth: true
          }
        },
        ...newsRoutes,
        ...artistsRoutes,
        ...concertsRoutes,
        ...albumsRoutes,
        {
          component: Login,
          name: 'Login',
          path: 'login',
        },
        {
          component: Register,
          name: 'Register',
          path: 'register',
        },
        {
          component: Search,
          path: 'search'
        }
      ]
    },
  ]
})

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(route => route.meta.auth)) {
    try {
      const isAuthorized = await userApi.verifyUser()

      if (!isAuthorized) {
        return next('/login')
      }

      return next()
    } catch (e) {
      localStorage.removeItem('token')
      return next('/login')
    }
  } else {
    return next()
  }
})

export default router
