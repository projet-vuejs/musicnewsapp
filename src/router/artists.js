import Index from '@/views/artists/Index.vue'
import Show from '@/views/artists/Show.vue'
import Create from '@/views/artists/Create.vue'
import Edit from '@/views/artists/Edit.vue'
import IndexFront from '@/views/artists/IndexFront.vue'
import ShowFront from '@/views/artists/ShowFront.vue'

const routes = [
  {
    path: 'admin/artists',
    name: 'admin.artists.index',
    component: Index,
  },
  {
    path: 'admin/artists/create',
    name: 'admin.artists.create',
    component: Create,
  },
  {
    path: 'admin/artists/:id/edit',
    name: 'admin.artists.edit',
    component: Edit,
  },
  {
    path: 'admin/artists/:id',
    name: 'admin.artists.show',
    component: Show,
  },
  {
    path: '/artists',
    name: 'artists.index',
    component: IndexFront,
  },
  {
    path: '/artists/:id',
    name: 'artists.show',
    component: ShowFront,
  },
]

export default routes
