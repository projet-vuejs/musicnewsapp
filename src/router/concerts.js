import Index from '@/views/concerts/Index.vue'
import Show from '@/views/concerts/Show.vue'
import Create from '@/views/concerts/Create.vue'
import Edit from '@/views/concerts/Edit.vue'
import IndexFront from '@/views/concerts/IndexFront.vue'

const routes = [
  {
    path: 'admin/concerts',
    name: 'admin.concerts.index',
    component: Index,
  },
  {
    path: 'admin/concerts/create',
    name: 'admin.concerts.create',
    component: Create,
  },
  {
    path: 'admin/concerts/:id/edit',
    name: 'admin.concerts.edit',
    component: Edit,
  },
  {
    path: 'admin/concerts/:id',
    name: 'admin.concerts.show',
    component: Show,
  },
  {
    path: '/concerts',
    name: 'concerts.index',
    component: IndexFront,
  }
]

export default routes
