import Vuex from 'vuex'
import Vue from 'vue'
import news from '@/store/modules/news'
import artists from '@/store/modules/artists'
import concerts from '@/store/modules/concerts'
import albums from '@/store/modules/albums'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    news,
    artists,
    concerts,
    albums
  }
})
