import axios from 'axios'

const state = {
  manyAlbums: [],
  album: {},
}

const getters = {
  getManyAlbums: state => state.manyAlbums,
  getAlbum: state => state.album,
}

const mutations = {
  setManyAlbums (state, manyAlbums) {
    state.manyAlbums = manyAlbums
  },
  setAlbum (state, album) {
    state.album = album
  }
}

const actions = {
  async fetchManyAlbumsByTerm ({ commit }, term) {
    const { data } = await axios.get(`http://localhost:3000/albums?name=${term}&_limit=8`)
    commit('setManyAlbums', data)
  },
  async fetchManyAlbums ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/albums')
    commit('setManyAlbums', data)
  },
  async fetchAlbum ({ commit }, id) {
    const albumData = await axios.get(`http://localhost:3000/albums/${id}`)
    const albumArtist = await axios.get(`http://localhost:3000/artists/${albumData.data.artistId}`)

    const data = {
      target: albumData.data,
      artist: albumArtist.data
    }

    commit('setAlbum', data)
  },
  async createAlbum ({ commit }, data) {
    await axios.post(`http://localhost:3000/albums`, {
      ...data
    })
  },
  async editAlbum ({ commit }, data) {
    await axios.patch(`http://localhost:3000/albums/${data.id}`, {
      ...data
    })
  },
  async deleteAlbum ({ commit }, id) {
    await axios.delete(`http://localhost:3000/albums/${id}`)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
