import axios from 'axios'

const state = {
  manyArtists: [],
  artist: {},
  genres: [],
}

const getters = {
  getManyArtists: state => state.manyArtists,
  getArtist: state => state.artist,
  getGenres: state => state.genres,
}

const mutations = {
  setManyArtists (state, manyArtists) {
    state.manyArtists = manyArtists
  },
  setArtist (state, artist) {
    state.artist = artist
  },
  setGenres (state, genres) {
    state.genres = genres
  }
}

const actions = {
  async fetchGenres ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/genres')
    commit('setGenres', data)
  },
  async fetchManyArtists ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/artists')
    commit('setManyArtists', data)
  },
  async fetchArtist ({ commit }, id) {
    const artistData = await axios.get(`http://localhost:3000/artists/${id}`)
    const artistGenre = await axios.get(`http://localhost:3000/genres/${artistData.data.genreId}`)
    const artistAlbums = await axios.get(`http://localhost:3000/albums?artistId=${artistData.data.id}`)
    const artistConcerts = await axios.get(`http://localhost:3000/concerts?artistId=${artistData.data.id}`)

    const data = {
      target: artistData.data,
      genre: artistGenre.data,
      albums: artistAlbums.data,
      concerts: artistConcerts.data,
    }

    commit('setArtist', data)
  },
  async createArtist ({ commit }, data) {
    await axios.post(`http://localhost:3000/artists`, {
      ...data
    })
  },
  async editArtist ({ commit }, data) {
    await axios.patch(`http://localhost:3000/artists/${data.id}`, {
      ...data
    })
  },
  async deleteArtist ({ commit }, id) {
    await axios.delete(`http://localhost:3000/artists/${id}`)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
