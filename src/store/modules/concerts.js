import axios from 'axios'

const state = {
  manyConcerts: [],
  concert: {}
}

const getters = {
  getManyConcerts: state => state.manyConcerts,
  getConcert: state => state.concert,
}

const mutations = {
  setManyConcerts (state, manyConcerts) {
    state.manyConcerts = manyConcerts
  },
  setConcert (state, concert) {
    state.concert = concert
  }
}

const actions = {
  async fetchManyConcerts ({ commit }) {
    const { data } = await axios.get('http://localhost:3000/concerts')
    commit('setManyConcerts', data)
  },
  async fetchConcert ({ commit }, id) {
    const concertData = await axios.get(`http://localhost:3000/concerts/${id}`)
    const concertArtist = await axios.get(`http://localhost:3000/artists/${concertData.data.artistId}`)

    const data = {
      target: concertData.data,
      artist: concertArtist.data
    }

    commit('setConcert', data)
  },
  async createConcert ({ commit }, data) {
    await axios.post(`http://localhost:3000/concerts`, {
      ...data
    })
  },
  async editConcert ({ commit }, data) {
    await axios.patch(`http://localhost:3000/concerts/${data.id}`, {
      ...data
    })
  },
  async deleteConcert ({ commit }, id) {
    await axios.delete(`http://localhost:3000/concerts/${id}`)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
